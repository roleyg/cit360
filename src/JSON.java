package JSON;
// Jackson library utilities
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
//Setup class
public class JSON {
    //setup our JSON info to enable converting of our info to a JSON string
    public static String familyToJSON (Family Family) {

        ObjectMapper mapper = new ObjectMapper();
        String a = "";

        try {
            a = mapper.writeValueAsString(Family);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }

        return a;
    }

    public static Family JSONtoFamily (String a) {

        ObjectMapper mapper = new ObjectMapper();
        Family Family = null;

        try {
            Family = mapper.readValue(a, Family.class);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }
        return Family;
    }

    public static void main(String[] args) {
        //Provide data for output
        Family fam = new Family();
        fam.setFirstName("Gareth");
        fam.setLastName("Roley");
        fam.setAge(30);

        String json = JSON.familyToJSON(fam);
        System.out.println(json);

        Family fam2 = JSON.JSONtoFamily(json);
        System.out.println(fam2);
    }

}